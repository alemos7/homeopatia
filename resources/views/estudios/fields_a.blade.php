<!-- Tipo Field -->
@if(!isset($estudios))
    <div class="row">
        <div class="form-group col-sm-12">
            <label for=""><b>{{ _i('Tipo') }}:</b> {{ _i('Animal') }}</label>
            <input style="display: none" type="text" id="tipo"   name="tipo" value="12">
{{--            <select class="form-control" name="tipo" id="tipo" required--}}
{{--                    title="{{_i('Nombre/s:Los datos deben ser cargados en la planilla de consulta sin acentos o simbolos. Debe ingresarse el texto con las letras puras correspondientes (no agregar letras con ´, ¨, ^, \', `, ~, etc.).')}}">--}}
{{--                <option value="97" @if(@old("tipo")==97) selected @endif>{{ _i('Humano') }}</option>--}}
{{--                <option value="12" @if(@old("tipo")==12) selected @endif>{{ _i('Animal') }}</option>--}}
{{--            </select>--}}
            {!! $errors->first('tipo', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>
    </div>

@endif


<div id="EAnimal">
    <div class="row">
        <!-- H Nombre Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Especie') }}:</b></label>
            {!! Form::text('a_especie', null, ['class' => 'form-control'. ( $errors->has('a_especie') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('a_especie', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>


        <!-- H Apellido Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Nombre del Dueño') }}:</b></label>
            {!! Form::text('a_duenio', null, ['class' => 'form-control'. ( $errors->has('a_duenio') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('a_duenio', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>


        <!-- H Identifica Field -->
        <div class="form-group col-sm-4">
            <label for=""><b>{{ _i('Nombre del Animal') }}:</b></label>
            {!! Form::text('a_animal', null, ['class' => 'form-control'. ( $errors->has('a_animal') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('a_animal', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>

    </div>

    <div class="row">

        <!-- H Iniciales Field -->
        <div class="form-group col-sm-6">
            <label for=""><b>{{ _i('Iniciales') }}:</b></label>
            {!! Form::text('a_iniciales', null, ['maxlength'=> '2', 'class' => 'form-control'. ( $errors->has('a_iniciales') ? ' is-invalid' : '' )]) !!}
            {!! $errors->first('a_iniciales', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}
        </div>
        @if(!isset($estudios))
{{--            <div class="form-group col-sm-6">--}}
{{--                <label for="fecha_animal"><b>{{ _i('Fecha de Nacimiento') }}:</b></label>--}}
{{--                <input class="form-control {{$errors->has('fecha_animal') ? ' is-invalid' : ''}}" id="fecha_animal" name="fecha_animal" type="date" value="{{($fecha)? $fecha : ''}}">--}}
{{--                {!! $errors->first('fecha_animal', '<span class="invalid-feedback"><strong>:message</strong></span>') !!}--}}
{{--            </div>--}}

        <?php
            if(app()->getLocale() == "es") {
//                echo "ES<hr>"; die();
                ?>
                <div class="form-group col-sm-6">
                    <div style="margin-bottom: -25px;"  class="row">
                        <div class="form-group col-sm-12">
                            <label for="fecha_humano"><b>{{ _i('Fecha de Nacimiento') }}:</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_dia" id="a_dia" required="" title="">
                                <option>{{ _i('Dia') }}</option>
                                <?php
        //                        echo $dia;
                                if (!isset($dia)) $dia = null;

                                for ($i = 1; $i <= 31; $i++) {
                                ?>
                                <option <?php if($i == $dia ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_mes" id="a_mes" required="" title="">
                                <option>{{ _i('Mes') }}</option>
                                <?php
                                if (!isset($mes)) $mes = null;
                                for ($i = 1; $i <= 12; $i++) {
                                ?>
                                <option <?php if($i == $mes ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_anio" id="a_anio" required="" title="">
                                <option>{{ _i('Año') }}</option>
                                <?php
                                if (!isset($anio)) $anio = null;
                                for ($i = date('Y'); $i >= 1920; $i--) {
                                ?>
                                <option <?php if($i == $anio ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }else{
//                echo "EN<hr>";
                ?>
                <div class="form-group col-sm-6">
                    <div style="margin-bottom: -25px;"  class="row">
                        <div class="form-group col-sm-12">
                            <label for="fecha_humano"><b>{{ _i('Fecha de Nacimiento') }}:</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_anio" id="a_anio" required="" title="">
                                <option>{{ _i('Dia') }}</option>
                                <?php
                                if (!isset($anio)) $anio = null;
                                for ($i = date('Y'); $i >= 1920; $i--) {
                                ?>
                                <option <?php if($i == $anio ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_mes" id="a_mes" required="" title="">
                                <option>{{ _i('Mes') }}</option>
                                <?php
                                if (!isset($mes)) $mes = null;
                                for ($i = 1; $i <= 12; $i++) {
                                ?>
                                <option <?php if($i == $mes ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <select class="form-control" name="a_dia" id="a_dia" required="" title="">
                                <option>{{ _i('Dia') }}</option>
                                <?php
                                if (!isset($dia)) $dia = null;
                                for ($i = 1; $i <= 31; $i++) {
                                ?>
                                <option <?php if($i == $dia ) { ?> selected <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
        @endif
    </div>

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <input type="hidden" id="ip" name="ip" value="127.0.0.1">
    <input type="hidden" id="user_agent" name="user_agent" value="Firefox">

    <button type="submit" class="btn btn-success" onclick="return confirm('{{_i('¿Esta Realmente seguro de crear este Estudio con estos Datos?')}}')">{{_i('Realizar Estudio')}}</button>
    <a href="{!! route('estudios.index') !!}" class="btn btn-success">{{ _i('Cancelar') }}</a>
</div>

@section('scripts')
    <script>
        var isSafari = window.safari !== undefined;
        if (isSafari) {
            //document.getElementById('fecha_humano').type = 'text';
            //document.getElementById('fecha_animal').type = 'text';
            if ( $('[type="date"]').prop('type') != 'date' ) {
                $('[type="date"]').datepicker();
            }
        }


        $(document).ready(function () {



            let estudioTipo = '{!! (isset($estudios->tipo))? $estudios->tipo : 0 !!}';

            if(estudioTipo!=0){
                if (estudioTipo == 'humano') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if (estudioTipo == 'animal') {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            }else{
                if ($('#tipo').val() == '97') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if ($('#tipo').val() == '12') {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            }

            $('#tipo').change(function () {

                if ($('#tipo').val() == '97') {
                    $('#EHumano').css('display', 'block');
                    $('#EAnimal').css('display', 'none');
                } else if ($('#tipo').val() == '12') {

                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'block');
                    document.getElementById("EAnimal").style.display = "block";
                } else {
                    $('#EHumano').css('display', 'none');
                    $('#EAnimal').css('display', 'none');
                }
            });
        });
    </script>
@endsection
